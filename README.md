# Formulario Dimamico BildChile

Hola como estas, puedes desarrollar en la plataforma que que te sientas a gusto, o la que estas postulando.

### [API] Desarrollar una api Rest con el siguiente metodo:
```sh
http://x.x.x.x:3000/getforms
```
Consideraciones:

1. Se requiere autentificacion para poder ejectutar geform
2. La autentificacion debe ser oAuth, oAuth2
3. El EndPoint debe retornar lo siguiente

```json
{
  status: 200,
  error: '',
  msg: {
    forms: [
      {
        name: 'formulario comentario',
        inputs: [
          {
            name: 'nombre',
            type: 'text', // input de tipo text
            required: true
          },
          {
            name: 'Email',
            type: 'email', // input y keyboard de tipo email 
            required: true
          },
          {
            name: 'Comentario',
            type: 'text',
            required: true
          }
        ] 
      }
    ] 
  }
}
```
***Guardar en ejercicios en GitHub, documentados en una rama llamada API***

# [Mobile]

1. Debes autentificarte con la apirest y ejecutar el endpoint
2. si te das cuenta el JSON retorna un array de formularios, es en el cual debes trabajar. debes hacer lo siguiente.
   1. Guardar el Json en persistencia. (con el fin de no depender de internet)
   2. Generar un listado con los siguientes datos (cargar lista de persistencia)
      1. Titulo: name
      2. Subtitulo: email
3. Crear nuevo registro y almacenar en persistencia (los campos estan especificados en el JSON).
1. estos campos deben generarse en tiempo de ejecucion, por lo cual se debe recorrer el json y generar los input:
```
 {
        name: 'formulario comentario',
        inputs: [
          {
            name: 'nombre',
            type: 'text', // input de tipo text
            required: true
          },
          {
            name: 'Email',
            type: 'email', // input y keyboard de tipo email 
            required: true
          },
          {
            name: 'Comentario',
            type: 'text',
            required: true
          }
        ] 
      }
```
3. Guardar en ejercicios en GitHub, documentados en una rama llamada ejemplo

***Si tienen dudas o consultas , arodriguez@bildchile.com con gusto los ayudare***


